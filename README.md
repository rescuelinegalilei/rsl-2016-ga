##rsl-2016-ga
#ReScueLine 2016 Galilei

This is the repository where we are developing the _code_ that will guide our robot at the 2017 edition of the Robocup JR in the Rescue Line competition.

![Alt text]( "Our robot")

##Commits
###Prefixes
|prefix	|meaning	|
|-------|---------------|
|[s]	|school commit	|
|[m]	|minor changes	|
|[M]	|Big changes	|


##Code standards

###Indentation
+ Indentations are made of two spaces

###Comments
Comments should respect these parameters:

+ the language used is English
+ the infinitive is preferred in order to keep things as simple as possible

The structure used to describe a function is as follows:

	/*******************************************
	* function name (possible parameters)
	* the aim of the function
	* possible values returned
	*******************************************/

* * *

##Contributors

+ jkm99zambo
+ gbrugna
+ Eleonora
+ new people

;)