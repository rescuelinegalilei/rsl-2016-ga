#include <Sensor.h>

#include "Arduino.h"

#include <Wire.h>

#include <string.h>

#define TCAADDR 0x70



void sensor::TcaSelect(uint8_t i) {

  if (i > 7) return;



  Wire.beginTransmission(TCAADDR);

  Wire.write(1 << i);

  Wire.endTransmission();

}



void sensor::init(){

  ToF.init();  //Initialization of ToF sensor

  ToF.setTimeout(500);

  // lower the return signal rate limit (default is 0.25 MCPS)

  ToF.setSignalRateLimit(0.1);

  // increase laser pulse periods (defaults are 14 and 10 PCLKs)

  ToF.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);

  ToF.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);

}



void sensor::SelectToF(String position){

  if(position=="Front"){

    sensor::TcaSelect(0);

  }

  else if(position=="SxUp"){

    sensor::TcaSelect(2);

  }

  else if(position=="SxDown"){

    sensor::TcaSelect(3);

  }

  else if(position=="BackUp"){

    sensor::TcaSelect(4);

  }

  else if(position=="BackDown"){

    sensor::TcaSelect(5);

  }

}



void sensor::SetToFDistanceReading(int distance,String position){
//sensor::SelectToF(position);

  if(distance<90){

    ToF.setSignalRateLimit(0.25);

    ToF.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 14);

    ToF.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 10);

  }

  else{

    ToF.setSignalRateLimit(0.1);

    ToF.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);

    ToF.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);

  }

}



int sensor::ReadToFmm(String position){

  sensor::SelectToF(position);

  int distance;

  int letture[2];

  inizioReadToFmm:;

  for (int i=0;i<2;i++){

      letture[i]=ToF.readRangeSingleMillimeters();

  }

  if(!(letture[0]-letture[1]<precision && letture[1]-letture[0]<precision)){

    goto inizioReadToFmm;

  }

  return (letture[0]+letture[1])/2;

}
