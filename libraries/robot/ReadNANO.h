#ifndef ReadNANO_H
#define ReadNANO_H

#include "Arduino.h"

class ReadNANO {
  public:
    int readNANO(int &o,int &v);
};

#endif
