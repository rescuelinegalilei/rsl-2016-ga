#include <ReadNANO.h>

int ReadNANO::readNANO(int &o,int &v) {
  char chr;
  while (Serial2.available()){
    chr=Serial2.read();}                          //Clean the buffer
  String str="";
  while(true) {
    chr=Serial2.read();
    if (chr=='#')                             //Search # character
      break;
  }
  while(chr!=',') {                        //Read chars and add them to a string while a comma isn't found
    if(Serial2.available()) {
      chr=Serial2.read();
      str+=chr;
    }
  }
  o=str.toInt();                        //Convert the string to an int
  o+=180;                               //Add to the int 180 because raw data are mapped in -180,+180
  str="";
  while(chr!='#') {
    if(Serial2.available()) {               //Does the previous things
      chr=Serial2.read();
      str+=chr;
    }
  }
  v=str.toInt();
  v+=180;
}
