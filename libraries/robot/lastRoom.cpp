#include <ArduinoSTL.h>
#include <Sensor.h>
#include <Move.h>

//initializing classes
//Move FinalMovements;
sensor FinalSensors;

//variables for the measurements taken every centimeter
struct Pos
{
  int x_distanceFromOppositeWall;
  int y;
  int triangle;
};

//evacuation zone locations and variable
enum evacuationZone
{
  LEFT_LOW,
  LEFT_HIGH,
  RIGHT_HIGH,
  NOT_FOUND
};
short evac;

//////////////////////////////////////////////
//TOGLIERE ROBOT SIZE A QUASI TUTTE LE MISURE
/////////////////////////////////////////////


//"Front..." means side with the pincher
//"Back.." means the side without the pincher

/***********************************************************************************
* void lastRoom()
* takes no parameters
* scans the room every cm (more or less) and takes a set of
  measurments for every scan. Locates the evacuation zone and the
  victims accordingly.
***********************************************************************************/
bool isBall(String position, Move &FinalMovements){  //La funzione accetta come parametro Side o Front a seconda di dove si vuole controllare
  int R=FinalSensors.ReadToFmm(position+"Down"); //Il raggio
  int side=5;                                    //Il diametro della pallina
  int angle=2*asin(side/(2*R));                  //L'angolo, calcolato trigonometricamente
  bool result;                                   //Variabile che verrà ritornata
  FinalMovements.rotateTo(FinalMovements.rotateFor(angle*1.3), 3, 30);  //Va a destra
  result=!FinalSensors.ReadToFmm(position+"Down")/10<FinalSensors.ReadToFmm(position+"Up")/10;  //Assegna true se la distanza è uguale o maggiore
  FinalMovements.rotateTo(FinalMovements.rotateFor(-2*angle*1.3), 3, 30);  //Va a sinistra
  if (result)  //Solo se ancora crede che sia pallina
    result=!FinalSensors.ReadToFmm(position+"Down")/10<FinalSensors.ReadToFmm(position+"Up")/10;  //Riassegna risultato
  while(FinalSensors.ReadToFmm(position+"Down")/10>FinalSensors.ReadToFmm(position+"Up")/10){
    FinalMovements.clock(30);   //Torna al punto in cui vede la differenza
  }
  FinalMovements.stop(); //Si ferma
  return result;   //Ritorna il risultato
}

void lastRoom(Move &FinalMovements)
{
using namespace std;


//entrance & side variables
vector<Pos> measure;
int robot_width = 13; //value has to be measured
int robot_length = 19;
int entering_side = FinalSensors.ReadToFmm("FrontUp") / 10;
int total_measurements;
int max_distance;
evac = NOT_FOUND;

if (entering_side > 119 - robot_length && entering_side < 121 - robot_length)
{
  total_measurements = entering_side - 1;
  max_distance = FinalSensors.ReadToFmm("SideUp") / 10;
  if (max_distance < 89 - robot_width)
    {
      evac = RIGHT_HIGH;
      max_distance = 90 - robot_width;
    }
    else
      evac = LEFT_HIGH;
}
// triangle in front of the entrance
else if (entering_side < 119 - robot_size)
{
  evac = LEFT_LOW;
  total_measurements = entering_side - 1;
  max_distance = 90 - robot_size;
}

measure.resize(total_measurements);

//navigation variables
int checkForVictim;
int distance_traveled;
int distanceToVictim;
int distanceToWall;
int victimsTaken = 0;

for (int i = 0; i < total_measurements; ++i)
{
  if (i = 0)
  {
    //check for a victim in front of the entrance
    checkForVictim = FinalSensors.ReadToFmm("FrontDown");
    if (checkForVictim < entering_side)
    {
      //go to the victim
      while (distanceToVictim <= checkForVictim - 3)
      {
        distanceToVictim = FinalSensors.ReadToFmm("FrontDown") / 10;
        FinalMovements.setspeeds(20, 20);
      }
      FinalMovements.stop();
      //lower  & raise arm  (comment to be replaced by actual code)

      //go back to entrance
      while (distanceToWall <=  entering_side)
      {
        distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
        FinalMovements.setspeeds(-20, -20);
      }
      FinalMovements.stop();
      distanceToWall = 0;
      distanceToVictim = 0;
      ++victimsTaken;
    }

    measure[i].x_distanceFromOppositeWall = entering_side;
    measure[i].y = FinalSensors.ReadToFmm("SideDown") / 10;
    measure[i].triangle = FinalSensors.ReadToFmm("SideUp") / 10 ;
    if (measure[i].y < max_distance - 1 && measure[i].y < measure[i].triangle)
      {
        //go to the victim
        FinalMovements.rotateTo(FinalMovements.rotateFor(90), 1, 20);
        while (distanceToVictim <= measure[i].y - 3)
        {
          distanceToVictim = FinalSensors.ReadToFmm("FrontDown") / 10;
          FinalMovements.setspeeds(20, 20);
        }
        FinalMovements.stop();
        //lower  & raise arm  (comment to be replaced by actual code)

        while (distanceToWall >= 2)
        {
          distanceToWall = FinalSensors.ReadToFmm("BackDown") / 10;
          FinalMovements.setspeeds(-20, -20);
        }
        FinalMovements.stop();
        distanceToWall = 0;
        FinalMovements.rotateTo(FinalMovements.rotateFor(-90), 3, 30);
        distanceToVictim = 0;
        ++victimsTaken;
      }
    }
  else
  {
    measure[i].x_distanceFromOppositeWall = FinalSensors.ReadToFmm("FrontUp") / 10 ;
    measure[i].y = FinalSensors.ReadToFmm("SideDown") / 10;
    measure[i].triangle = FinalSensors.ReadToFmm("SideUp") / 10;
    if (measure[i].y < max_distance - 1 && measure[i].y < measure[i].triangle)
      {
        //go to the victim
        FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
        while (distanceToVictim <= measure[i].y - 3)
        {
          distanceToVictim = FinalSensors.ReadToFmm("FrontDown") / 10;
          FinalMovements.setspeeds(20, 20);
        }
        FinalMovements.stop();
        //lower  & raise arm  (comment to be replaced by actual code)

        while (distanceToWall >= 2)
        {
          distanceToWall = FinalSensors.ReadToFmm("BackDown") / 10;
          FinalMovements.setspeeds(-20, -20);
        }
        FinalMovements.stop();
        distanceToWall = 0;
        FinalMovements.rotateTo(FinalMovements.rotateFor(-90), 3, 30);
        distanceToVictim = 0;
        ++victimsTaken;
        if (victimsTaken > 3)
        {
            switch (evac)
            {
              case LEFT_LOW:
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              //lower  & raise arm  (comment to be replaced by actual code)
              //lower "cargo back" (comment to be replaced by actual code)
              victimsTaken = 0;
              while (distanceToWall <= measure[i].x_distanceFromOppositeWall)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(-20, -20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              break;

              case LEFT_HIGH:
              FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              FinalMovements.rotateTo(FinalMovements.rotateFor(-90), 3, 30);
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              //lower  & raise arm  (comment to be replaced by actual code)
              //lower "cargo back" (comment to be replaced by actual code)
              victimsTaken = 0;
              while(distanceToWall >= entering_side - measure[i].x_distanceFromOppositeWall)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(-20, -20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              FinalMovements.rotateTo(FinalMovements.rotateFor(-90), 3, 30);
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
              break;

              case RIGHT_HIGH:
              FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              FinalMovements.stop();
              distanceToWall = 0;
              FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              distanceToWall = 0;
              FinalMovements.stop();
              //lower  & raise arm  (comment to be replaced by actual code)
              //lower "cargo back" (comment to be replaced by actual code)
              victimsTaken = 0;
              FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
              while(distanceToWall >= 2)
              {
                distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              distanceToWall = 0;
              FinalMovements.stop();
              FinalMovements.rotateTo(FinalMovements.rotateFor(90), 3, 30);
              while (distanceToWall >= measure[i].x_distanceFromOppositeWall)
              {
                distanceToWall = distanceToWall = FinalSensors.ReadToFmm("FrontUp") / 10;
                FinalMovements.setspeeds(20, 20);
              }
              distanceToWall = 0;
              FinalMovements.stop();
              break;
            }
          }
         }
      }
   //move 1 cm
   while (distance_traveled <= measure[i].x_distanceFromOppositeWall - 1)
   {
     distance_traveled = FinalSensors.ReadToFmm("FrontUp") / 10;
     FinalMovements.setspeeds(20, 20);
   }
   FinalMovements.stop();
   }
}
