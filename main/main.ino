#include <NewPing.h>
#include <LineFinder.h>
#include <Move.h>
#include <PID.h>
#include <Servo.h>
#include <ReadNANO.h>
#include <VL53L0X.h>
#include <Wire.h>
#include <Sensor.h>
#include <lastRoom.h>

/*************************************/
//         GLOBAL CONSTANTS          //
/*************************************/

int basespeed = 9; //minimum speed [percentage]

#define pinA_1 4        //Motors' PINS 
#define pinA_2 7
#define pinB_1 6
#define pinB_2 5

#define maxspeed 60    //maximum speed in percentage

#define maxValue 5000   //maximum value readable by light sensors

#define FRONTTRIGGER 49

#define FRONTECHO 48

#define MAXSONARDISTANCE 200

#define MINOBSTACLEDISTANCE 5

/*************************************/



/*************************************/
//        GLOBAL VARIABLES           //
/*************************************/

float error;
int pidVal;
int cases;
int conGreen = 0;

/*************************************/


/*************************************/
//         GLOBAL ARRAYS             //
/*************************************/

int line_pins[8] = {26, 27, 28, 29, 30, 31, 32, 33};
long times[8];
long minvals[8] = {296, 220, 220, 256, 256, 328, 292, 364};
long maxvals[8] = {2092, 1528, 1572, 1644, 1472, 1776, 1980, 2632};
float rap[8];
/*************************************/





/************************************/
//             GREEN                //
/************************************/

const float constantGreenSX = 0.4;
const float constantGreenDX = 0.4;


const byte sclPin = A7;   // pin scl                 PINS MUSTN'T BE CHANGED
const byte sdaPin = A4;   // pin sda



const byte sclPin2 = A5;  // pin scl
const byte sdaPin2 = A6;  // pin sda


bool isGreenSX = false;      //1 SX, 2 DX
bool isGreenDX = false;

/************************************/





/*************************************/
//       CLASSES INITIALIZATION      //
/*************************************/

LineFinder line;
PID pidous;
Move movements;
sensor Sensors;
NewPing FrontSonar(FRONTTRIGGER, FRONTECHO, MAXSONARDISTANCE);

/************************************/


void isGreen() {  //1 SX, 2 DX

  if (isGreenDX) {

    movements.rotateTo(movements.rotateFor(90), 3, basespeed);

  }
  else {

    movements.rotateTo(movements.rotateFor(-90), 3, basespeed);

  }

}



/*******************************************
  obstacle
  pass an obstacle
*******************************************/

void obstacle() {
  movements.stop();
  movements.rotateTo(movements.rotateFor(-30), 3, basespeed);
  movements.setspeeds(basespeed, basespeed);
  delay(750);
  movements.stop();
  movements.rotateTo(movements.rotateFor(30), 3, basespeed);
  movements.setspeeds(basespeed, basespeed);
  delay(600);
  movements.rotateTo(movements.rotateFor(90), 3, basespeed);
  movements.setspeeds(basespeed, basespeed);
  delay(100);
  line.ReadSensors(times);
  line.FindError(times, rap, &cases);

  while (cases == 1) {

    line.ReadSensors(times);

    line.FindError(times, rap, &cases);

  }

  movements.rotateTo(movements.rotateFor(-90), 3, basespeed);

  movements.setspeeds(-basespeed, -basespeed);

  delay(1000);

}



/*******************************************
    isobstacle

    decide if there is an obstacle

   and return it as a boolean

*******************************************/

bool isobstacle() {

  movements.sprint();

  delay(500);


  movements.setspeeds(-basespeed, -basespeed);

  delay(30);

  movements.stop();
  if (Sensors.ReadToFmm("Front") > MINOBSTACLEDISTANCE)

    return true;

  return false;

}



void setup() {
  Serial.begin(9600);   //Serial for Debug
  Serial2.begin(9600);  //Serial for NANO board
  //Serial.println("Begin Setup");
  Wire.begin();   //Initialization of I2C
  movements.init(pinA_1, pinA_2, pinB_1, pinB_2, maxspeed); //Initialization of Move class with motors' pins and Maxspeed
  line.init(line_pins, maxValue, minvals, maxvals);      //Initialization of Linefinder class with pins of line array and value of calibration
  pidous.init(28, 3, 1);                                  //Initialization of PID library with the values of PID

  //Sensors.init();                               //Initializing of object to use various sensors

  //Serial.println("End Setup");

}



void loop() {

  line.ReadSensors(times);                          //Function that fill vector times with the reads of the array
  error = line.FindError(times, rap, &cases);       //Find the error and the case where we are
  if (cases == 0) {                                //If case is 0 we are following the line
    // //Serial.print("Linea: ");
    pidVal = pidous.Compute(error);               //Computing the error with values of PID to regulate the speed of the motors
    if (pidVal > 0) {
      movements.setspeeds(basespeed + pidVal, basespeed - pidVal * 1.2); //setting the speed thanks to the computed value
    }
    else {
      movements.setspeeds(basespeed + pidVal * 1.2, basespeed - pidVal); //setting the speed thanks to the computed value
    }
  }

  else if (cases == 1) {                    //If cases is 1 there is no line under the robot
    ////Serial.println("Bianco");
    movements.sprint();
  }
  else {                               //If cases is 3 we have only line under the light sensor (so we are over an intersection, so we have to understand if there is a green safe path to follow or if we have to go forward)
    //Serial.println("Intersenzione: ");
    movements.stop();                  //We stop
    isGreenSX = false;                    //and set the variables of green to false
    isGreenDX = false;
    if (conGreen < 5) {             //We will check 5 times per intersection if there is the green safe path
      movements.setspeeds(-basespeed, -basespeed);    //we go back slowly in order to read ...
      for (int timef = millis() + 400; millis() < timef && !isGreenSX && !isGreenDX;) { //For 400 millis (WE HAVE TO CHANGE THIS TIME) times we...  (Even if get_ColorsX finds green it stops)
        get_Colors();                                 //... check if the robot sees a green
        get_Colors2();
      }
      conGreen++;                                     //We set the variabile with an higher value
      if (isGreenSX || isGreenDX) {                         //If a green is seen
        conGreen = 0;                                 //We reset the variable
        movements.setspeeds(basespeed, basespeed);
        delay(50);
        isGreen();                            //we choose to go with isGreen(), which will decide how to rotate the robot (considering the values of isGreen**)
      }
      //Else we return to the begin of loop
    }
    else {                                //If we have checked 5 times that there is no green we ignore the intersection
      movements.setspeeds(basespeed, basespeed);
      delay(50);
      conGreen = 0;
    }
  }
}
